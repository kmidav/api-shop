<?php
namespace App\Service;
use App\Entity\DailyReport;
use App\Repository\ProductOfferRepository;

class ReportHelper
{
    private $productOfferRepository;
    public function __construct(ProductOfferRepository $productOfferRepository)
    {
        $this->productOfferRepository = $productOfferRepository;
    }
    /**
     * @param array An array of criteria to limit the results
     *              Supported keys are:
     *                  * from DateTimeInterface
     *                  * to   DateTimeInterface
     * @return array|DailyReport[]
     */
    public function fetchMany(int $limit = null, int $offset = null, array $criteria = [])
    {
        $fromDate = $criteria['from'] ?? null;
        $toDate = $criteria['to'] ?? null;
        $stats = [];
        $i = 0;

        foreach ($this->fetchStatsData() as $statData) {
            $i++;
            if ($offset >= $i) {
                continue;
            }

            $dateString = $statData['date'];
            $date = new \DateTimeImmutable($dateString);

            if ($fromDate && $date < $fromDate) {
                continue;
            }

            if ($toDate && $date > $toDate) {
                continue;
            }

            $stats[] = $this->createStatsObject($statData);

            if ($limit && count($stats) >= $limit) {
                break;
            }
        }

        return $stats;
    }
    public function fetchOne(string $date): ?DailyReport
    {
        foreach ($this->fetchStatsData() as $statData) {
            if ($statData['date'] === $date) {
                return $this->createStatsObject($statData);
            }
        }
        return null;
    }
    public function count(): int
    {
        return count($this->fetchStatsData());
    }
    private function fetchStatsData(): array
    {
        $statsData = json_decode(file_get_contents(__DIR__.'/fake_stats.json'), true);
        return $statsData['stats'];
    }
    private function getRandomItems(array $items, int $max)
    {
        if ($max > count($items)) {
            shuffle($items);
            return $items;
        }
        $finalItems = [];
        while (count($finalItems) < $max) {
            $item = $items[array_rand($items)];
            if (!in_array($item, $finalItems)) {
                $finalItems[] = $item;
            }
        }
        return $finalItems;
    }
    private function createStatsObject(array $statData): DailyReport
    {
        $listings = $this->productOfferRepository
            ->findBy([], [], 10);
        return new DailyReport(
            new \DateTimeImmutable($statData['date']),
            $statData['visitors'],
            $this->getRandomItems($listings, 5)
        );
    }
}