<?php


namespace App\DataTransformer;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\ProductOfferInput;
use App\Entity\ProductOffer;

class ProductOfferInputDataTransformer implements DataTransformerInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param ProductOfferInput $input
     * @param string $to
     * @param array $context
     * @return object|void
     */
    public function transform($input, string $to, array $context = [])
    {
        $this->validator->validate($input);

        if (isset($context[AbstractItemNormalizer::OBJECT_TO_POPULATE])) {
            $productOffer = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];
        } else {
            $productOffer = new ProductOffer($input->name);
            $productOffer->setOwner($input->owner);
        }

        $productOffer->setDescription($input->description)
            ->setPrice($input->price)
            ->setIsPublished($input->isPublished);

        return $productOffer;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof ProductOffer) {
            return false;
        }

        return $to === ProductOffer::class && ($context['input']['class'] ?? null) === ProductOfferInput::class;
    }
}