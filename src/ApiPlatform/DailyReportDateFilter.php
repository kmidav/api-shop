<?php


namespace App\ApiPlatform;


use ApiPlatform\Core\Serializer\Filter\FilterInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DailyReportDateFilter implements FilterInterface
{
    public const FROM_FILTER_CONTEXT = 'daily_reports_from';
    private bool $throwOnInvalid;
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger, bool $throwOnInvalid = false)
    {
        $this->throwOnInvalid = $throwOnInvalid;
        $this->logger = $logger;
    }

    public function apply(Request $request, bool $normalization, array $attributes, array &$context)
    {
        $from = $request->query->get('from');
        if (!$from) {
            return;
        }

        $fromDate = \DateTimeImmutable::createFromFormat('Y-m-d', $from);

        if (!$fromDate && $this->throwOnInvalid) {

            throw new BadRequestHttpException('invalid from date format');
        }

        if ($fromDate) {
            $fromDate->setTime(0, 0, 0);
            $this->logger->info('Filtering from date %s', $from);

            $context[self::FROM_FILTER_CONTEXT] = $fromDate;
        }
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'search' => [
                'property' => null,
                'type' => 'string',
                'required' => false,
                'openapi' => [
                    'description' => 'From date e.g. 2020-11-28 format'
                ]
            ]
        ];
    }
}