<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\ProductOffer;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ProductOfferDataPersister implements DataPersisterInterface
{
    /**
     * @var DataPersisterInterface
     */
    private $decoratedDataPersister;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(DataPersisterInterface $decoratedDataPersister, EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->decoratedDataPersister = $decoratedDataPersister;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function supports($data): bool
    {
        return $data instanceof ProductOffer;
    }

    /**
     * @param ProductOffer $data
     * @return object|void
     */
    public function persist($data)
    {
        $originalData = $this->entityManager->getUnitOfWork()->getOriginalEntityData($data);

        $wasAlreadyPublished = $originalData['isPublished'] ?? false;
        if ($data->getIsPublished() && !$wasAlreadyPublished) {
            // todo отправлять письмо сдесь но лучше это сделать именно у нового товара а не у оффера
            $this->logger->info('offer is published');
        }

        $this->decoratedDataPersister->persist($data);
    }

    public function remove($data)
    {
        $this->decoratedDataPersister->remove($data);
    }
}