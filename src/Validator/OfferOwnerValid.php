<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class OfferOwnerValid extends Constraint
{
    public $message = 'Can not set owner to another user {{ value }}';

    public $anonymousMessage = 'You can not create offer unless you are authenticated';
}
