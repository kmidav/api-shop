<?php

namespace App\Validator;

use App\Entity\ProductOffer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class PublishedProductOfferValidValidator extends ConstraintValidator
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Security
     */
    private $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\PublishedProductOfferValid */
        /** @var ProductOffer $value */
        if (!$value instanceof ProductOffer) {
            throw new \LogicException('only above ProductOffer you can place it!');
        }
        // если пустое
        if (null === $value || '' === $value) {
            return;
        }

        $originalData = $this->entityManager->getUnitOfWork()->getOriginalEntityData($value);

        $wasAlreadyPublished = $originalData['isPublished'] ?? false;

        if ($value->getIsPublished() === $wasAlreadyPublished) {
            return;
        }

        if ($value->getIsPublished()) {
            // todo проверять на валидность у товара а не у оффера
            if ((strlen($value->getDescription()) < 10) && !$this->security->isGranted('ROLE_ADMIN')) {
                $this->context->buildViolation('Cannot publish, because description is too short!')
                    ->atPath('description')
                    ->addViolation();
            }

            return;
        }

        if (!$this->security->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException('Only admin users can unpublish!');
        }
    }
}
