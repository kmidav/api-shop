<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Security;

class SetIsMeOnCurrentUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function onRequestEvent(RequestEvent $event)
    {

        if (!$event->isMasterRequest()) {
            return;
        }
        // todo убрать лучше
//        /** @var User $user */
//        $user = $this->security->getUser();
//
//        if (!$user) {
//            return;
//        }

        // ставим поле isMe true если мы зареганы
        //$user->setIsMe(true);
    }

    public static function getSubscribedEvents()
    {
        return [
            RequestEvent::class => 'onRequestEvent',
        ];
    }
}
