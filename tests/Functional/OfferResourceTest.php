<?php


namespace App\Tests\Functional;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\ProductOffer;
use App\Entity\User;
use App\Test\BaseApiTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class OfferResourceTest extends BaseApiTestCase
{
    use ReloadDatabaseTrait;

    public function testCreateOffer(): void
    {
        $client = self::createClient();

        $client->request('POST', '/api/product/offers', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => ['fake' => 'value'],
        ]);

        self::assertResponseStatusCodeSame(401);


        $authenticatedUser = $this->createUserAndLogin($client, 'user1@mail.ru', 'user1', '123');
        self::assertResponseStatusCodeSame(204);


        $offerData = [
            'name' => 'newOffer',
            'description' => 'about',
            'price' => 8000
        ];
        $client->request('POST', '/api/product/offers', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => $offerData
        ]);

        self::assertResponseStatusCodeSame(201, 'offer created');


        $offerData = [
            'name' => 'newOffer',
            'description' => 'about',
            'price' => 8000
        ];
        $user2 = $this->createUser('user2@mail.ru', 'user2', '123');
        $client->request('POST', '/api/product/offers', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => $offerData + ['owner' => '/api/users/'.$user2->getId()]
        ]);
        self::assertResponseStatusCodeSame(422, 'wrong owner');


        $client->request('POST', '/api/product/offers', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => $offerData + ['owner' => '/api/users/'.$authenticatedUser->getId()]
        ]);
        self::assertResponseStatusCodeSame(201);
    }

    public function testUpdateOffer(): void
    {
        $client = self::createClient();
        $user1 = $this->createUser('user1@mail.ru', 'user1', '123');

        $offer = new ProductOffer('offer1');
        $offer->setOwner($user1)
            ->setPrice('10000')
            ->setDescription('desc1')
            ->setIsPublished(true);

        $em = $this->getEntityManager();
        $em->persist($offer);
        $em->flush();

        $this->login($client, 'user1@mail.ru', '123');

        $client->request('PUT', '/api/product/offers/'.$offer->getId(), [
            'json' => ['title' => 'update']
        ]);
        self::assertResponseStatusCodeSame(200);


        $user2 = $this->createUser('user2@mail.ru', 'user2', '123');
        $this->login($client, 'user2@mail.ru', '123');
        $client->request('PUT', '/api/product/offers/'.$offer->getId(), [
            'json' => ['title' => 'update']
        ]);

        self::assertResponseStatusCodeSame(403);



        $client->request('PUT', '/api/product/offers/'.$offer->getId(), [
            'json' => [
                'title' => 'update',
                'owner' => '/api/users/'.$user1->getId()
            ]
        ]);

        self::assertResponseStatusCodeSame(403);
    }

    public function testGetOfferCollection(): void
    {
        $client = self::createClient();

        $user = $this->createUser('user1@mail.ru', 'user1', '123');

        $offer1 = new ProductOffer('offer1');
        $offer1->setOwner($user)
            ->setPrice(1000)
            ->setDescription('offer1')
            ->setIsPublished(true);

        $offer2 = new ProductOffer('offer2');
        $offer2->setOwner($user)
            ->setPrice(1000)
            ->setDescription('offer2')
            ->setIsPublished(true);

        $offer3 = new ProductOffer('offer3');
        $offer3->setOwner($user)
            ->setPrice(1000)
            ->setDescription('offer3');

        $em = $this->getEntityManager();
        $em->persist($offer1);
        $em->persist($offer2);
        $em->persist($offer3);
        $em->flush();

        $client->request('GET', '/api/product/offers');
        self::assertJsonContains(['hydra:totalItems' => 2]);
    }

    public function testGetOfferItem(): void
    {
        $client = self::createClient();

        $user = $this->createUserAndLogin($client,'user1@mail.ru', 'user1', '123');

        $offer1 = new ProductOffer('offer1');
        $offer1->setOwner($user)
            ->setPrice(1000)
            ->setDescription('offer1')
            ->setIsPublished(false);

        $em = $this->getEntityManager();
        $em->persist($offer1);
        $em->flush();

        $client->request('GET', '/api/product/offers/'.$offer1->getId());
        self::assertResponseStatusCodeSame(200);


        $client->request('GET', '/api/users/'.$user->getId());
        $data = $client->getResponse()->toArray();

        self::assertEmpty($data['productOffers']);
    }

    public function testPublishOffer(): void
    {
        $client = self::createClient();
        $user1 = $this->createUser('user1@mail.ru', 'user1', '123');
        $offer = new ProductOffer('offer1');
        $offer->setOwner($user1)
            ->setPrice('10000')
            ->setDescription('some textsome textsome textsome textsome textsome textsome textsome textsome text')
            ->setIsPublished(false);
        $em = $this->getEntityManager();
        $em->persist($offer);
        $em->flush();

        $this->login($client, 'user1@mail.ru', '123');

        $client->request('PUT', '/api/product/offers/'.$offer->getId(), [
            'json' => ['isPublished' => true]
        ]);

        self::assertResponseStatusCodeSame(200);

        /** @var ProductOffer $offer */
        $offer = $em->getRepository(ProductOffer::class)->find($offer->getId());

        self::assertTrue($offer->getIsPublished());
        // todo проверить что отправилось письмо юзеру
    }

    public function testPublishProductOfferValidation()
    {
        // todo проверять это у Product а не у оффера
        // todo проверять что опубликованный оффер валиден т.е. код товара присутствует и т.д.
    }
}